def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a<=0 and b<=0 and c<=0:
        triangle = False
    else:
        if a+b>c and a+c>b and c+b>a:
            triangle = True
        else:
            triangle = False

    return triangle
